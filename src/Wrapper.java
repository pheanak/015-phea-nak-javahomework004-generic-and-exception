public class Wrapper<E> {
    wrapperList<E> list = new wrapperList<>();
    public void addItem(E value) throws addValueFormatException{

        if (value==null){
        throw new   addValueFormatException("Inputted null value");
    }else if (list.contains(value)){
        throw new   addValueFormatException("Duplicate value : "+value);
    }
        else
                list.addItem(value);
}

    public E getItem(int i){
        return list.getItem(i);
    }

    public int size() {
        return list.size();
    }
}
