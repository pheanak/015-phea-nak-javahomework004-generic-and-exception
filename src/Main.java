
public class Main {
    public static void main(String[] args) {
        Wrapper<Integer> list = new Wrapper<>();
        try {
            list.addItem(1);
            list.addItem(null);
        } catch (addValueFormatException e) {
            System.out.println(e);
        }
        System.out.println("\nList all from inputted");
        for (int i=0;i<list.size();i++){
            System.out.println(list.getItem(i));
        }
    }
}

